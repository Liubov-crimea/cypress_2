class Login {

    btnProfile(){
        return cy.get('.header__profile')
    }

    email(){
        return cy.get('input[type="text"]')
    }

    password(){
        return cy.get('input[type="password"]')  
    }

    clickBtnLogin(){
        return cy.contains("Увійти")  
    }

    logout(){
        return cy.get('a[class="header-drop_item logout-link"]') 
    }

    
}

export default Login