describe("Log in", function(){

    beforeEach(() => {
        cy.visit('https://kasta.ua/uk/')
        cy.wait(4000)  
        //ua language
        cy.get('div[class="header__lang top_menu_item"]').click
        cy.get('a[href="/lang/uk"]').should('to.have.text', 'Українська')
     })
    
    it("main elements is displayed", function(){
        cy.location('protocol').should('eq', 'https:')
        cy.title().should('eq', 'Інтернет-магазин Kasta ⋆ 1 млн товарів ⋆ від одягу до техніки ⋆ Київ, Одеса, Харків, Дніпро, Львів')
        cy.get('.header-logo__img').should('be.visible') //logo is displayed
        cy.get('div[class="flex header__search-container ml-2"]').should('be.visible') //chearsh bar is displayed
    //class="menu__catalog" //catalog of products
    }) 

    it("check input search", function(){
    const product = 'капелюх'
    cy.get('input[type="search"]').type(`${product}`).type('{enter}')  //submit() not work
    //cy.url().should('include', 'https://kasta.ua/uk/market/shlyapa/') 

    //! all results have search product
    cy.get('div[class="list-render flex wrap"]').each(($card) => {  
          const description = $card.find('header[class="p__info_name"]').text()
          expect(description).to.include(`${product}`)
          cy.log('All results includes key word')
        }) 
    //sort prodyct by orange color and buy Шляпа Braxton
    cy.get('div[style="background-color:#FF8310"]').click({force: true}) 
    cy.get('div[class="list-render flex wrap"] article[id="14066178:641"]').click()
    cy.get('button[id="productBuy"]').click()

    //check of backet (products on this test must be add)
    cy.get('.header_basket').click()
    cy.get('a[class="cart_item_name"]').should('have.text', 'Шляпа Braxton')
 
    }) 
    
    it("check buy from catalog", function(){
        cy.get('ul[class="menu-vertical__list"]>li').eq(3).click()
        cy.contains('Пінетки').click({force: true})
        cy.get('h1[class="lh-24 t-18 py-0 mt-3 w-100"]').should('have.text', 'Красиві дитячі пінетки для малюків')
        //фильтры
        cy.get('div[style="background-color:#D2B990"]').click()
        cy.get('input[class="filter__search-input search_input bg-light mb-3"]').type('Papulin')
        cy.contains('Сезонність').click({force: true})
        cy.contains('Туреччина').click({force: true})
        //пинетки 
        cy.get('div[class="list-render flex wrap"] article[id="14899670:582"]').click()
        cy.get('h1[class="p__pads p__title p__name p__dsc-order-1 m-0"]')
        .should('have.text', 'Пiнетки Papulin бежеві повсякденні')
        cy.get('label[for="18"]').click()
        cy.get('button[id="productBuy"]').click()
        

        //check of backet (products on this test must be add)
        cy.get('.header_basket').click()
        cy.get('a[class="cart_item_name"]').should('have.text', 'Пiнетки Papulin')

        //delete this product
        cy.get('span[class="close"]').click()
        //check cart is empty
        cy.get('a[class="btn empty-cart_main"]').should('exist')

       })

     
    })   
       



    /*sort by increasing price
    cy.get('div[class="text-input lh-30 bg-white pl-3 pr-2 flex center"]').click()
    cy.get('a[class="block lh-32 ws-nowrap "]').eq(0).should('be.visible').click() 

    //beret
    cy.get('a[href="/uk/market/beret/"]').click()
    cy.title().should('include', 'Берет')
    //for women
    cy.get('label[id="OmFmZmlsaWF0aW9uemhpbmthbQ"]') */




    //buy 

    //check in basket
    //cy.get('span[class="header_basket-title"]').click

    
    //
    

    


    
    
        