import Login from "./Page Obj/LoginPage_for_kasta" 

const email = 'lubov-crimea@ukr.net'
const password = '345ert'

describe("Log in", function(){

    const login = new Login()

    it("Sing in", function(){
        cy.visit('https://kasta.ua/uk/')
        cy.wait(4000)
        cy.location('protocol').should('eq', 'https:')
        cy.title().should('eq', 'Інтернет-магазин Kasta ⋆ 1 млн товарів ⋆ від одягу до техніки ⋆ Київ, Одеса, Харків, Дніпро, Львів')

        //login
        login.btnProfile().click()

        login.email()
        .type(`${email}`)
        .should('have.value', 'lubov-crimea@ukr.net')

        login.clickBtnLogin()
        .should('be.visible')
        .click()

        login.password()
        .type(`${password}`)
        .should('have.value', '345ert')

        login.clickBtnLogin()
        .should('be.visible')
        .click()

        cy.wait(6000)
        
        //check login
        cy.get('[for="ff660493-c2bb-a810-60d4-50753dffcedf"]').click()
        login.btnProfile().click()
        cy.url().should('include', 'https://kasta.ua/uk/me/')
        cy.wait(5000)
    

        //logout
        login.logout().click({ force: true })

        //check logout
        cy.get('.auth_title').should('have.text', 'Вхід / Реєстрація')
        
    })
})

