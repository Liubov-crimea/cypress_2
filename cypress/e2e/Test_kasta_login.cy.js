describe("Log in", function(){
    it("Sing in", function(){
        cy.visit('https://kasta.ua/uk/')
        cy.wait(3000)

        cy.get('.header__profile').click()

        cy.get('input[type="text"]').type('lubov-crimea@ukr.net')
        //cy.get('button[type="submit"]').click()
        cy.contains("Увійти").click()

        cy.get('input[type="password"]').type('345ert')
        cy.contains("Увійти").click()

        cy.wait(6000)

        //check login
        cy.get('[for="ff660493-c2bb-a810-60d4-50753dffcedf"]').click()
        cy.get('.header__profile').click()
        cy.url().should('include', 'https://kasta.ua/uk/me/')
        cy.wait(5000)

        //logout
        cy.get('a[class="header-drop_item logout-link"]').click({ force: true })

        //check logout
        cy.get('.auth_title').should('have.text', 'Вхід / Реєстрація')
       


        
    })
})